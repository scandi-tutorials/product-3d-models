# 3D Models for Products in Scandi

Renders a "3D Models" tab in the Product Details Page, containing interactive product 3D models.

Implemented using [model-viewer](https://modelviewer.dev/).