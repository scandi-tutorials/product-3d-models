export const _getProductInterfaceFields = (args, callback, instance) => {
  const originalFields = callback(...args);
  return [...originalFields, "model_3d_urls"];
};

export default {
  "Query/ProductList": {
    "member-function": { _getProductInterfaceFields },
  },
};
