import Product3DViewer from "../component/Product3DViewer/Product3DViewer.component";

export const PRODUCT_3D_MODEL_TAB = "PRODUCT_3D_MODEL_TAB";

export const tabMap = (member, instance) => {
  return {
    ...member,
    [PRODUCT_3D_MODEL_TAB]: {
      name: __("3D Models"),
      shouldTabRender: () => {
        const { product: { model_3d_urls = [] } = {} } = instance.props;
        console.log(instance.props);

        // For some reason, shouldTabRender needs to return the opposite of what you would think
        return !(model_3d_urls.length > 0);
      },
      render: (key) => {
        const { product: { model_3d_urls = [] } = {} } = instance.props;

        return <Product3DViewer key={key} modelUrls={model_3d_urls} />;
      },
    },
  };
};

export default {
  "Route/ProductPage/Component": {
    "member-property": { tabMap },
  },
};
