import PropTypes from "prop-types";
import { PureComponent } from "react";

import "./Product3DViewer.style";

function loadModelViewer() {
  const MODULE_URL =
    "https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js";

  const script = document.createElement("script");
  script.src = MODULE_URL;
  script.type = "module";

  document.head.appendChild(script);
}

loadModelViewer();

export class Product3DViewer extends PureComponent {
  static propTypes = {
    modelUrls: PropTypes.arrayOf(PropTypes.string),
  };

  state = {
    activeModelIndex: 0,
  };

  renderModelSwitcher() {
    const { modelUrls } = this.props;
    const { activeModelIndex } = this.state;

    const activeNumber = activeModelIndex + 1;
    const size = modelUrls.length;

    const isFirst = activeModelIndex <= 0;
    const isLast = activeModelIndex >= size - 1;

    if (size < 2) {
      return null;
    }

    return (
      <div block="Product3DViewer" elem="Switcher">
        <button
          block="Product3DViewer"
          elem="Button"
          disabled={isFirst}
          onClick={() =>
            this.setState({ activeModelIndex: activeModelIndex - 1 })
          }
        >
          {__("Previous")}
        </button>
        <span block="Product3DViewer" elem="ActiveIndex">
          {activeNumber}
        </span>
        <button
          block="Product3DViewer"
          elem="Button"
          disabled={isLast}
          onClick={() =>
            this.setState({ activeModelIndex: activeModelIndex + 1 })
          }
        >
          {__("Next")}
        </button>
      </div>
    );
  }

  render() {
    const { modelUrls } = this.props;
    const { activeModelIndex } = this.state;

    const url = modelUrls[activeModelIndex];

    return (
      <div block="Product3DViewer">
        {this.renderModelSwitcher()}
        <model-viewer
          class="Product3DViewer-Model"
          src={url}
          alt={__("Product 3D Model")}
          auto-rotate
          camera-controls
        ></model-viewer>
      </div>
    );
  }
}

export default Product3DViewer;
