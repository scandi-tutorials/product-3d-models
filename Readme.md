# 3D Product Models for Scandi

Created as part of the [product 3D model extension tutorial for Scandi](https://docs.scandipwa.com/tutorials/product-3d-model-extension).

Consists of 2 modules:
- [Magento Backend with GraphQL Support](./product-3d-graph-ql)
- [Scandi Frontend Extension](./product-3d-scandi)
